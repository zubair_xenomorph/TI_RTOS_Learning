################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../CC2650_LAUNCHXL.cmd 

CFG_SRCS += \
../empty_min.cfg 

C_SRCS += \
../CC2650_LAUNCHXL.c \
../ccfg.c \
../empty_min.c 

OBJS += \
./CC2650_LAUNCHXL.obj \
./ccfg.obj \
./empty_min.obj 

C_DEPS += \
./CC2650_LAUNCHXL.d \
./ccfg.d \
./empty_min.d 

GEN_MISC_DIRS += \
./configPkg/ 

GEN_CMDS += \
./configPkg/linker.cmd 

GEN_OPTS += \
./configPkg/compiler.opt 

GEN_FILES += \
./configPkg/linker.cmd \
./configPkg/compiler.opt 

GEN_FILES__QUOTED += \
"configPkg\linker.cmd" \
"configPkg\compiler.opt" 

GEN_MISC_DIRS__QUOTED += \
"configPkg\" 

C_DEPS__QUOTED += \
"CC2650_LAUNCHXL.d" \
"ccfg.d" \
"empty_min.d" 

OBJS__QUOTED += \
"CC2650_LAUNCHXL.obj" \
"ccfg.obj" \
"empty_min.obj" 

C_SRCS__QUOTED += \
"../CC2650_LAUNCHXL.c" \
"../ccfg.c" \
"../empty_min.c" 


